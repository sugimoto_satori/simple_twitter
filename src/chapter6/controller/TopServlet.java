package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.CommentService;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {


        boolean isShowMessageForm = false;
        //sessionからログインユーザー情報を取り出す。
        User user = (User) request.getSession().getAttribute("loginUser");
        //ユーザーログイン時にisShowMessageFormにtrueを返す。
        if (user != null) {
            isShowMessageForm = true;
        }

        //入力された日付を基につぶやきの絞り込みを行う。
        String startDate = request.getParameter("start_date");
        String lastDate = request.getParameter("last_date");

        //userごとのつぶやきを取得する。
        String userId = request.getParameter("user_id");
        List<UserMessage> messages = new MessageService().select(userId, startDate, lastDate);

        //コメント情報を取得する。
        List<UserComment> comments = new CommentService().select();

        //messagesとisShowMessageFormをsessionに格納し、top.jspで参照できるようにする。
        request.setAttribute("messages", messages);
        request.setAttribute("comments", comments);
        request.setAttribute("isShowMessageForm", isShowMessageForm);
        request.getRequestDispatcher("/top.jsp").forward(request, response);

    }

}