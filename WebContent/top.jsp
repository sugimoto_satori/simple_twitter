<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>簡易Twitter</title>
    </head>
    <body>
		<div class="main-contents">

			<div class="header">
				<c:if test="${ empty loginUser }">
					<a href="login">ログイン</a>
					<a href="signup">登録する</a>
				</c:if>

				<c:if test="${ not empty loginUser }">
					<a href="./">ホーム</a>
					<a href="setting">設定</a>
					<a href="logout">ログアウト</a>
					<div class="profile">
						<div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
						<div class="account">@<c:out value="${loginUser.account}" /></div>
						<div class="description"><c:out value="${loginUser.description}" /></div>
					</div>

				</c:if>

			</div>

			<div class="body">

				<c:if test="${ not empty loginUser }">

					<!-- トップに追加されたつぶやき機能のフォーム。エラーがなければ表示する。 -->>
					<c:if test="${ not empty errorMessages }">
						<div class="errorMessages">
							<ul>
								<c:forEach items="${errorMessages}" var="errorMessage">
									<li><c:out value="${errorMessage}" />
								</c:forEach>
							</ul>
						</div>
					<c:remove var="errorMessages" scope="session" />
				</c:if>


				<div class="form-area">
					<c:if test="${ isShowMessageForm }">
						<form action="message" method="post">いま、どうしてる？<br />
							<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
							<br />
							<input type="submit" value="つぶやく">（140文字まで）
							<br /><br />
						</form>
					</c:if>
				</div>

				</c:if>

				<!-- 絞り込み処理 -->
				<div class="search">
					<form action="./" method="get">

						<!-- 開始日入力フォームの実装 -->
						<label for="name">開始日</label>
						<input type="date" name="start_date" value="${start_date}" id="start_date"/>
						<br />

						<!-- 終了日入力フォームの実装 -->
						<label for="name">終了日</label>
						<input type="date" name="last_date" value="${last_date}" id="last_date"/>
						<input type="submit" value="検索">

					</form><br /><br />
				</div>

				<!-- つぶやきの連続表示。つぶやきにはそれぞれ返信をぶら下げる -->
				<div class="messages">
					<c:forEach items="${messages}" var="message">
						<div class="message">
							<div class="account-name">
								<span class="account">
									<a href="./?user_id=<c:out value="${message.userId}"/> ">
										<c:out value="${message.account}" />
									</a>
								</span>
								<span class="name"><c:out value="${message.name}" /></span>
							</div>
							<pre class="text"><c:out value="${message.text}" /></pre>
							<div class="date">
								<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /><br>
							</div>

							<!-- 既存つぶやきの操作 -->
							<c:if test="${loginUser.id == message.userId}">

								<!-- 編集ボタンの実装 -->
								<form action="edit" method="get">
									<input name="id" value="${message.id}" id="id" type="hidden"/>
									<input type="submit" value="編集">
								</form>

								<!-- 削除ボタンの実装 -->
								<form action="deleteMessage" method="post">
									<input name="id" value="${message.id}" id="id" type="hidden"/>
									<input type="submit" value="削除">
								</form>
							</c:if>

							<br>
							<br>

							<!-- 返信フォームの実装。ログイン時のみ表示 -->
							<c:if test="${ not empty loginUser }">
								<form action="comment" method="post">返信<br />
									<input name="message_id" value="${message.id}" id="message_id" type="hidden"/>
									<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea><br />
									<input type="submit" value="返信"><br />
								</form>
							</c:if>

							<!-- 返信の連続表示 -->
							<c:forEach items="${comments}" var="comment">
								<c:if test="${comment.messageId == message.id}">
									<div class="comment">
										<div class="comment-account-name">
											<span class="account"><c:out value="${comment.userId}"/></span>
											<span class="name"><c:out value="${comment.name}" /></span>
										</div>
									<pre class="text"><c:out value="${comment.text}" /></pre>
										<div class="date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
									</div>
								</c:if>
							</c:forEach>

						</div>
    				</c:forEach>
				</div>

			</div>

            <br><div class="copyright"> Copyright(c)SatoriSugimoto</div>
        </div>
    </body>
</html>