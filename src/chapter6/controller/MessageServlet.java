package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.User;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//セッションにエラーメッセージを格納する。セッション作成
        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        //フォームで入力されたつぶやきを持ってくる。
        String text = request.getParameter("text");
        if (!isValid(text, errorMessages)) {
        	//エラーが発生しているときに限り、エラーメッセージをセッションに格納
            session.setAttribute("errorMessages", errorMessages);
            //リダイレクトして戻る。
            response.sendRedirect("./");
            return;
        }

        //エラーが発生していなければ、Beansを基にメッセージを作成。つぶやきをセット。
        Message message = new Message();
        message.setText(text);

        //セッションにおいてあるユーザー情報を引っ張ってくる。
        User user = (User) session.getAttribute("loginUser");
        //メッセージBeansにユーザーIDをセット（userインスタンスから持ってくる）。
        message.setUserId(user.getId());

        //Daoを呼び出すメソッドを呼び出し、メッセージをサーバーに送信する。リダイレクトして戻る。
        new MessageService().insert(message);
        response.sendRedirect("./");
    }

    private boolean isValid(String text, List<String> errorMessages) {
    	//入力されたtextの内容を精査し、エラーメッセージを入力する。
        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}