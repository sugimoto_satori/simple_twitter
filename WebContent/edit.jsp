<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
   		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>編集画面</title>
    </head>

    <body>


		<c:if test="${ not empty loginUser }">

			<div class="form-area">
				<form action="edit" method="post">つぶやき<br />
					<textarea name="text" cols="100" rows="5" class="tweet-box">${text}</textarea><br />
					<input name="id" value="${id}" id="id" type="hidden"/>
					<input type="submit" value="更新"><br />

					<a href="./">戻る</a>
				</form>

			</div>

		</c:if>

		<div class="copyright"> Copyright(c)SatoriSugimoto</div>

	</body>
</html>