package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();

            //Daoを通じてサーバーを操作する。問題あれば戻す。
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //DBに保存されたメッセージを表示する。
    //userIdを引数として受け取り、userIdごとのメッセージを表示する。
    public List<UserMessage> select(String userId, String startDate, String lastDate) {
    	//表示するメッセージの上限を設定
    	final int LIMIT_NUM = 1000;
    	Connection connection = null;

        try {

        	//日付の指定がない場合の分岐
        	if(StringUtils.isBlank(startDate)) {
        		startDate = "2000-01-01 00:00:00";
        	} else {
        		startDate += " 00:00:00";
        	}

        	if(StringUtils.isBlank(lastDate)) {
        		//現在日時の取得
        		long time = new Date().getTime();
            	Timestamp ts = new Timestamp(time);
        		lastDate = String.valueOf(ts);
        	} else {
        		lastDate += " 23:59:59";
        	}

            connection = getConnection();

            //userIdの有無で分岐
            Integer id = null;
            if(!StringUtils.isBlank(userId)) {
            	id = Integer.parseInt(userId);
            }

            //つぶやきを上限1000件で取得する。
            List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, startDate, lastDate);
            commit(connection);
            return messages;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //メッセージの削除メソッド
    public void delete(Message message) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		//Daoから削除メソッドを呼び出せ
    		new MessageDao().delete(connection, message);
    		commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //メッセージの取得メソッド
    public Message selectText(String id) {

    	Connection connection = null;

    	try {
    		connection = getConnection();

    		//Daoから取得メソッドを呼び出せ
    		Message message =new MessageDao().select(connection, id);
    		commit(connection);

    		return message;
          } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //メッセージの更新メソッド
    public void update(Message message) {

    	Connection connection = null;

    	try {
    		connection = getConnection();

    		//Daoから更新メソッドを呼び出せ
    		new MessageDao().update(connection, message);
    		commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}