package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Message;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("    user_id, ");
            sql.append("    text, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                  // user_id
            sql.append("    ?, ");                  // text
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");   // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getUserId());
            ps.setString(2, message.getText());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //つぶやき削除命令
    public void delete (Connection connection, Message message) {

    	PreparedStatement ps = null;
    	try {
    		//代入されたidを持つメッセージを削除せよ
    		String sql = "DELETE  FROM messages WHERE id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, message.getId());

            ps.executeUpdate();

    	} catch(SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //つぶやき取得命令
    public Message select (Connection connection, String id) {

    	PreparedStatement ps = null;
    	try {
    		//代入されたidを持つメッセージを取得せよ
    		String sql = "SELECT * FROM messages WHERE id =? ";

    		ps = connection.prepareStatement(sql);
    		ps.setString(1, id);

            ResultSet rs = ps.executeQuery();
            List<Message> messages = toMessages(rs);

            if (messages.isEmpty()) {
                return null;
            } else {
            	//つぶやきの情報を返す。
            	//リストにはつぶやきが一つしかないので、0を返せばOK。
                return messages.get(0);
            }

    	} catch(SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //DBから取得したメッセージをリストに入れる。
    private List<Message> toMessages(ResultSet rs) throws SQLException {

        List<Message> messages = new ArrayList<>();
        try {
            while (rs.next()) {
            	//Message情報を入力フォームから受け取る。
                Message message = new Message();
                message.setId(rs.getInt("id"));
                message.setUserId(rs.getInt("user_id"));
                message.setText(rs.getString("text"));
                message.setCreatedDate(rs.getTimestamp("created_date"));
                message.setUpdatedDate(rs.getTimestamp("updated_date"));

                //message情報をリストに入れる
                messages.add(message);
            }
            //リストの内容を返す。
            return messages;
        } finally {
            close(rs);
        }
    }



    //つぶやき更新命令
    public void update (Connection connection, Message message) {

    	PreparedStatement ps = null;
    	try {
    		//代入されたidを持つメッセージを更新せよ
    		String sql = "UPDATE messages SET text = ? WHERE id = ? ";

    		ps = connection.prepareStatement(sql);
    		ps.setString(1, message.getText());
    		ps.setInt(2, message.getId());

            ps.executeUpdate();

    	} catch(SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}

