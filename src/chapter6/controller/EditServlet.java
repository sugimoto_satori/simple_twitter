package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();


    	//DBからつぶやき情報を取得して、edit.jspに渡す。
    	String id = request.getParameter("id");
    	Message message =null;

    	//idの中身が空でなく、かつidが数字であるときにだけ既存のmessageを取得する。
    	if(!StringUtils.isBlank(id) && id.matches("^[0-9]*$")) {
    		//その上で存在しないidが指定された場合はnullが返ってくる。
            message = new MessageService().selectText(id);
    	}

    	//messageがnullであれば、idは空か、数字でないか、存在しないかのいずれかに該当する。
    	//messageがnullか否かを判定する。
    	if(message == null) {
    		errorMessages.add("不正なパラメータが入力されました");
    		session.setAttribute("errorMessages",errorMessages);
    		response.sendRedirect("./");
    		return;
    	}

    	//requestにつぶやきのidと更新後のつぶやきをセットしておく。
    	request.setAttribute("id", message.getId());
    	request.setAttribute("text", message.getText());
    	request.getRequestDispatcher("edit.jsp").forward(request,response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//セッションから入力されたテキストと更新するつぶやきのidを取得する。
    	HttpSession session = request.getSession();
    	List<String> errorMessages = new ArrayList<>();
    	String updatedText = request.getParameter("text");
    	String id = request.getParameter("id");

    	//エラー処理
    	if(!isValidText(updatedText,errorMessages)) {
    		session.setAttribute("errorMessages",errorMessages);
    		request.getRequestDispatcher("edit.jsp").forward(request,response);
    		return;
    	}

    	//更新したメッセージの設定
    	Message updatedMessage = new Message();
    	updatedMessage.setId(Integer.parseInt(id));
    	updatedMessage.setText(updatedText);

    	//メッセージの更新
    	new MessageService().update(updatedMessage);
    	response.sendRedirect("./");

    }

    private boolean isValidText(String text, List<String> errorMessages) {
    	//入力されたtextの内容を精査し、エラーメッセージを入力する。
        if (StringUtils.isBlank(text)) {
            errorMessages.add("入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }

        return true;
    }


}